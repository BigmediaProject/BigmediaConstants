module.exports = {
  auth: {
    Action: require('./lib/auth_action.json'),
    Provider: require('./lib/auth_provider.json'),
    AccessType: require('./lib/auth_accessType.json')
  },
  camp: {
    PosterType: require('./lib/camp_posterType.json'),
    CampaignType: require('./lib/camp_campaignType.json'),
    PhotoType: require('./lib/camp_photoType.json')
  },
  fin: {
    DocumentType: require('./lib/fin_documentType.json'),
    DocumentFileFormat: require('./lib/fin_documentFileFormat.json')
  }
}

