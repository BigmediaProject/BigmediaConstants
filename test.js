const {
  auth: {
    Action,
    Provider,
    AccessType
  },
  camp: {
    PosterType,
    CampaignType,
    PhotoType
  },
  fin: {
    DocumentType,
    DocumentFileFormat
  }
} = require('./index')
console.assert(Action.approvePayments === 42)
console.assert(Provider.facebook === 1)
console.assert(AccessType.full === 2)
console.assert(PosterType.Flex === 2)
console.assert(CampaignType.Social === 2)
console.assert(DocumentType.Tax === 351)
console.assert(DocumentFileFormat.pdf === 345)
console.assert(PhotoType.Night === 2)
